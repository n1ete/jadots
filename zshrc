#  _____    _
# |__  /___| |__  _ __ ___
#   / // __| '_ \| '__/ __|
#  / /_\__ \ | | | | | (__
# /____|___/_| |_|_|  \___|

#########
# zplug #
#########

source /usr/share/zsh/scripts/zplug/init.zsh

zplug "mafredri/zsh-async", from:github
zplug "sindresorhus/pure", use:pure.zsh, from:github, as:theme
zplug "zsh-users/zsh-syntax-highlighting"
zplug "zsh-users/zsh-history-substring-search"
zplug "chrissicool/zsh-256color"
zplug "jreese/zsh-titles"
zplug "plugins/vi-mode", from:oh-my-zsh
zplug "modules/git", from:prezto
zplug "pigmonkey/notes.sh", use:"notes.sh"

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load


###########
# History #
###########

setopt append_history
setopt extended_history
setopt hist_expire_dups_first
setopt hist_ignore_all_dups
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_reduce_blanks
setopt hist_save_no_dups
setopt hist_verify
setopt pushd_ignore_dups

# History Substring Search
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# Share history across all terminals.
setopt share_history

# Keep a ton of history.
HISTSIZE=100000
SAVEHIST=100000
HISTFILE=~/.zhistory
export HISTIGNORE="ls:cd:cd -:pwd:exit:date:* --help"

########
# Misc #
########

source ~/.aliases

# Add ruby gems to the path.
if which ruby >/dev/null && which gem >/dev/null; then
    path=($(ruby -e 'puts Gem.user_dir')/bin $path)
fi

# Use SSH completion for Mosh.
compdef mosh=ssh

# Menu completion
zstyle ':completion:*' menu select

# Prevent Pure from auto-pulling git repos.
PURE_GIT_PULL=0

# Activate dircolors
test -r "~/.dir_colors" && eval $(dircolors ~/.dir_colors)


######
# Vi #
######

bindkey -M viins 'kj' vi-cmd-mode
#bindkey -M vicmd "_" history-incremental-search-backward
#bindkey -M vicmd "-" history-incremental-search-forward
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down


#######
# FZF #
#######

if [ -r /usr/share/fzf/key-bindings.zsh ]; then
    source /usr/share/fzf/key-bindings.zsh
fi
if [ -r /usr/share/fzf/completion.zsh ]; then
    source /usr/share/fzf/completion.zsh
fi


########
# Pass #
########

# Setup alternative pass.
wpass() {
    PASSWORD_STORE_DIR="$ALTPASSDIR" pass "$@"
}
compdef -e 'PASSWORD_STORE_DIR=$ALTPASSDIR _pass' wpass

# Access pass
access() {
    PASSWORD_STORE_DIR="$ACCESSPASSDIR" pass "$@"
}
compdef -e 'PASSWORD_STORE_DIR=$ACCESSPASSDIR _pass' access


#########
# pyenv #
#########

if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi


# Show OS info when opening a new terminal
neofetch

## Font mode for powerlevel9k
#P9K_MODE="nerdfont-complete"
#
## Prompt elements
#P9K_LEFT_PROMPT_ELEMENTS=(custom_user dir vcs)
#P9K_RIGHT_PROMPT_ELEMENTS=(background_jobs go_version)
#
## Set name of the theme to load.
#ZSH_THEME="powerlevel9k/powerlevel9k"
#
## Prompt settings
#P9K_PROMPT_ON_NEWLINE=true
#P9K_RPROMPT_ON_NEWLINE=true
#P9K_MULTILINE_FIRST_PROMPT_PREFIX_ICON=$'%K{white}%k'
##P9K_MULTILINE_LAST_PROMPT_PREFIX_ICON=$'%K{green}%F{black} \uf155 %f%F{green}%k\ue0b0%f '
#
## Separators
#P9K_LEFT_SEGMENT_SEPARATOR_ICON=$'\ue0b0'
#P9K_LEFT_SUBSEGMENT_SEPARATOR_ICON=$'\ue0b1'
#P9K_RIGHT_SEGMENT_SEPARATOR_ICON=$'\ue0b2'
#P9K_RIGHT_SUBSEGMENT_SEPARATOR_ICON=$'\ue0b7'
#
## Dir colours
#P9K_DIR_HOME_BACKGROUND='black'
#P9K_DIR_HOME_FOREGROUND='white'
#P9K_DIR_HOME_SUBFOLDER_BACKGROUND='black'
#P9K_DIR_HOME_SUBFOLDER_FOREGROUND='white'
#P9K_DIR_DEFAULT_BACKGROUND='yellow'
#P9K_DIR_DEFAULT_FOREGROUND='black'
#P9K_DIR_SHORTEN_LENGTH=2
#P9K_DIR_SHORTEN_STRATEGY="truncate_from_right"
#
## OS segment
#P9K_OS_ICON_BACKGROUND='black'
#P9K_LINUX_ICON='%F{cyan} \uf303 %F{white} arch %F{cyan}linux%f'
#
## VCS icons
#P9K_VCS_GIT_ICON=$'\uf1d2 '
#P9K_VCS_GIT_GITHUB_ICON=$'\uf113 '
#P9K_VCS_GIT_GITLAB_ICON=$'\uf296 '
#P9K_VCS_BRANCH_ICON=$''
#P9K_VCS_STAGED_ICON=$'\uf055'
#P9K_VCS_UNSTAGED_ICON=$'\uf421'
#P9K_VCS_UNTRACKED_ICON=$'\uf00d'
#P9K_VCS_INCOMING_CHANGES_ICON=$'\uf0ab '
#P9K_VCS_OUTGOING_CHANGES_ICON=$'\uf0aa '
#
## VCS colours
##P9K_VCS_MODIFIED_BACKGROUND='blue'
##P9K_VCS_MODIFIED_FOREGROUND='black'
##P9K_VCS_UNTRACKED_BACKGROUND='green'
##P9K_VCS_UNTRACKED_FOREGROUND='black'
##P9K_VCS_CLEAN_BACKGROUND='green'
##P9K_VCS_CLEAN_FOREGROUND='black'
##
### VCS CONFIG
#P9K_VCS_SHOW_CHANGESET=false
#
## Status
#P9K_STATUS_OK_ICON=$'\uf164'
#P9K_STATUS_ERROR_ICON=$'\uf165'
#P9K_STATUS_ERROR_CR_ICON=$'\uf165'
#
## Battery
#P9K_BATTERY_LOW_FOREGROUND='red'
#P9K_BATTERY_CHARGING_FOREGROUND='blue'
#P9K_BATTERY_CHARGED_FOREGROUND='green'
#P9K_BATTERY_DISCONNECTED_FOREGROUND='blue'
#P9K_BATTERY_VERBOSE=true
#
## Programming languages
#P9K_RBENV_PROMPT_ALWAYS_SHOW=true
#P9K_GO_VERSION_PROMPT_ALWAYS_SHOW=true

# User with skull
#user_with_skull() {
#    echo -n "\ufb8a $(whoami)"
#}
#P9K_CUSTOM_USER="user_with_skull"
#
## Command auto-correction.
#ENABLE_CORRECTION="true"
#
## Command execution time stamp shown in the history command output.
#HIST_STAMPS="mm/dd/yyyy"
#DISABLE_LS_COLORS="true"
#
## Plugins to load
#plugins=(
#    colorize
#    copyfile
#    docker
#    docker-compose
#    emacs
#    git
#    gitfast
#    golang
#    history-substring-search
#    kubectl
#    rust
#    safe-paste
#    tmux
#    virtualenv
#    zsh-autosuggestions
#    zsh-completions
#    zsh-history-substring-search
#    zsh-syntax-highlighting
#)
#
#setopt HIST_IGNORE_SPACE
#autoload -U compinit && compinit
#source $ZSH/oh-my-zsh.sh

#complete -o nospace -C /usr/bin/vault vault
#complete -o nospace -C /usr/bin/terraform terraform
