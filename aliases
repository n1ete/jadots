########
# Core #
########

alias cp="rsync -ah --inplace --no-whole-file --info=progress2"
alias mkdir='mkdir -p'
alias grep='grep -i --color=always'
alias ls='ls --color=always'
alias l='ls -lh'         # Lists human readable sizes.
alias ll='l -A'          # Lists human readable sizes, hidden files.
alias lr='l -R'          # Lists human readable sizes, recursively.
alias lt='l -tr'         # Lists sorted by date, most recent last.
alias ip='ip -c'
alias rm='rm -i'
alias x='ranger'
alias c='cmus'
alias h='htop'

#############################
# dotfiles managed with git #
#############################
alias kobsdots='/usr/bin/git --git-dir=/home/n1ete/git/kobsdots/ --work-tree=/home/n1ete'
#alias docker='podman'
###########
# Udiskie #
###########

alias um='udiskie-mount -r'
alias uu='udiskie-umount'
alias up='um -p "builtin:tty"'

##############
# ArchiveBox #
##############
#alias bmark="eval export $(grep -v '^#' ~/.ArchiveBox.conf); ~/library/src/ArchiveBox/archive"

#############
# Git Annex #
#############

alias gx='git annex'
alias gxa='git annex add'
alias gxs='git annex sync'
alias gxg='git annex get'
alias gxd='git annex drop'
alias gxc='git annex copy'
alias gxe='git annex edit'

gxurl() {
    REPO="${1:-$HOME/documents}"
    less "$REPO"/.git/annex/url
}


###############
# Taskwarrior #
###############

alias t='task'
alias ta='task add'
alias tm='task modify'
alias to='taskopen'
alias ti='task add due:tomorrow tag:inbox'
tn() {
    if ! taskopen -a $1; then
        task annotate notes $1
        taskopen -a $1
    fi
}


##########
# Pacman #
##########

alias pacx="sudo pacman --remove"
alias pacX="sudo pacman --remove --nosave --recursive"


############
# My Repos #
############

alias lmr='mr --config $HOME/library/src/myrepos.conf --directory=$HOME/library/src'
alias wmr='mr --config $HOME/projects/jpsec/docs/config/myrepos.conf --directory=$HOME/projects/jpsec'
alias pmr='mr --config $HOME/projects/.mrconfig --directory=$HOME/projects'


########
# Pass #
########

alias cplogin='sed -n "/^login/Is/login:\s\+//p" | xsel -ib'
alias cpurl='sed -n "/^url/Is/url:\s\+//p" | xsel -ib'

_pw() {
    local characters="${1}" length="${2:-25}"
    bash -c 'read -r -n "$0" pass < <(LC_ALL=C tr -dc "$1" < /dev/urandom) && echo $pass' $length $characters
}
alias pw='_pw "[:graph:]"'
alias pw:a='_pw "[:alnum:]"'
alias pw:d='_pw "[:digit:]"'


########
# Misc #
########

alias mtr-report='mtr --report --report-cycles 10 --no-dns'
alias e='aunpack'
alias http-serve='python -m http.server'
alias bc='bc -lq'
alias utc='env TZ="UTC" date'

iowaiting() {
    watch -n 1 "(ps aux | awk '\$8 ~ /D/  { print \$0 }')"
}

def() {
    dict $1 | less
}

jul () {
    date -d "$1-01-01 +$2 days -1 day" "+%Y-%m-%d";
}
